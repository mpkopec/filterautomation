import numpy as np
import scipy.signal

class BodePlots():

	__lti_systems = {	'Chn1': [[1],[1,1]],
						'Chn2': [[0.1,2,0.3],[1,-0.1,3]],
						'Chn3': [[10,2],[-11,-6,3,4]],
						'Chn4': [[10,20],[-1,4]]}

	__freq_vector = []
	__jomega_vector = []
	__transfer_function = None
	__Hs = None

	def __init__(self, startFreq, stopFreq, steps, channel):
		self.__freq_vector = np.linspace(startFreq, stopFreq, steps)
		self.__jomega_vector = 2*np.pi*self.__freq_vector*1j

		self.__Hs = scipy.signal.lti(self.__lti_systems['Chn'+str(channel)][0],
									 self.__lti_systems['Chn'+str(channel)][1])
		self.__transfer_function = np.polyval(self.__Hs.num, self.__jomega_vector) / np.polyval(self.__Hs.den, self.__jomega_vector)

	def get_magnitude_plot(self):
		magnitude = 20.0*np.log10(abs(self.__transfer_function))
		return self.__freq_vector, magnitude

	def get_phase_plot(self):
		phase = np.arctan2(self.__transfer_function.imag, self.__transfer_function.real)*180.0/np.pi % 360
		return self.__freq_vector, phase



if __name__=='__main__':
	b = BodePlots(.01,100,10000,2)
	x,y = b.get_magnitude_plot()
	import matplotlib.pyplot as plt
	plt.semilogx(x,y)
	plt.show()