#!/usr/bin/env python 

from __future__ import division
import serial
#from Oscilloscope import Oscilloscope
import matplotlib.pyplot as plt
import numpy as np
from time import sleep

class SnapShot:
	
	#__channel = 0
	
	def __init__(self, oscilloscope = None, channel = 0):
		'''port = '/dev/ttyUSB0',\
				baudrate = 9600,\
				timeout = 1,\
				bitcount = serial.EIGHTBITS,\
				stopbits = serial.STOPBITS_ONE,\
				parity = serial.PARITY_NONE,\
				terminator = 1):
		Oscilloscope.__init__(self,\
							port,\
							baudrate,\
							timeout,\
							bitcount = serial.EIGHTBITS,\
							stopbits = serial.STOPBITS_ONE,\
							parity = serial.PARITY_NONE,\
							terminator = 1)'''
		self.__oscilloscope = oscilloscope
		self.__channel = channel
	
	def _getPK2pk(self,channel):
		setupCommands=[
			"MEASU:MEAS"+str(self.__channel)+":STATE ON",
			"MEASU:MEAS"+str(self.__channel)+":TYP PK2pk",
			"MEASU:MEAS"+str(self.__channel)+":VAL?"
		]
		self.__oscilloscope.sendMultipleCommands(setupCommands)
		PK2pk = float(self.__oscilloscope.readResponse())
		return PK2pk
	
	def _getDataset(self, channel):
		setupCommands=[
			"DATA:SOURCE CH"+str(channel),
			"DATA:ENCDG ASCI",
			"DATA:WIDTH 2",
			"DATA:START 1",
			"DATA:STOP 10000",
			"CURVE?"
		]
		self.__oscilloscope.sendMultipleCommands(setupCommands)
		YStream = self.__oscilloscope.readResponse().split(',')
		Ytmp = [float(data) for data in YStream]
		deltaY = max(Ytmp)-min(Ytmp)
		amplitude = self._getPK2pk(channel)
		YAxis = [item*amplitude/deltaY for item in Ytmp]
		return YAxis
	
	def _getTimeAxis(self, Ylen):
		self.__oscilloscope.sendCommand("WFMP:XIN?\n")
		_timeScale = float(self.__oscilloscope.readResponse())
		XAxis = np.linspace(0,_timeScale*10000000,Ylen)
		return XAxis
	
		
	def getSnapShot(self):
		_y = self._getDataset(self.__channel)
		_x = self._getTimeAxis(len(_y))
		
		return _x, _y
		


if __name__=='__main__':

	snp = SnapShot()
	snp.getSnapShot(1)
