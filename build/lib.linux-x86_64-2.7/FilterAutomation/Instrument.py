#!/usr/bin/env python

import serial

class Instrument():
	__connHandle = None
	__terminator = None
	__id = None

	def __init__(self, port = 0, baudrate = 9600, timeout = 1, bitcount = serial.EIGHTBITS, stopbits = serial.STOPBITS_ONE, parity = serial.PARITY_NONE, terminator = 1):
		try:
			self.__connHandle = serial.Serial(port, baudrate, timeout = timeout, stopbits = stopbits, parity = parity, bytesize = bitcount, dsrdtr = 0, xonxoff = 0, rtscts = 0)
		except (OSError, serial.SerialException):
			self.__connHandle = False

		self.__terminator = "\n" if terminator != 0 else "\r\n"

	def sendCommand(self, command):
		if self.__connHandle:
			self.__connHandle.write(command + self.__terminator)

	def sendMultipleCommands(self, commandList):
		if 'list' in repr(type(commandList)):
			for commandToSend in commandList:
				self.sendCommand(commandToSend)
		else:
			raise TypeError("Multiple commands should be arranged in lists!")

	def readResponse(self):
		return self.__connHandle.readline()

	def identify(self):
		self.sendCommand("*IDN?")
		return self.readResponse()

	def getID(self):
		self.sendCommand("*IDN?")
		self.__id = self.readResponse()
		return self.__id
	
	def test(self):
		pass

if __name__ == "__main__":
	print "Connecting to something that can reply on a command."
	print "On port 0."
	instr = Instrument()
	print instr.getID()
	print "Sending CH1:SCA 1; *IDN?\\n"
	clist = ['CH1:SCA 1', '*IDN?']
	instr.sendMultipleCommands(clist)
	print instr.readResponse()
