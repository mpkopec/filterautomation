#!/usr/bin/env python

from __future__ import division

import serial
import math
from Instrument import Instrument
from time import sleep


class Oscilloscope(Instrument):
	__channels = [1, 2, 3, 4]
	MAX_VOLTS_PER_DIV = 10

	def __init__(self, port = '/dev/ttyUSB1', baudrate = 9600):
		Instrument.__init__(self, port, baudrate, timeout = 1, bitcount = serial.EIGHTBITS, stopbits = serial.STOPBITS_ONE, parity = serial.PARITY_NONE, terminator = 1)
	
	def setVerticalScale(self, channel, volts):
		if channel in self.__channels:# and volts <= self.MAX_VOLTS_PER_DIV:
			self.sendCommand("CH" + str(channel) + ":SCA " + str(volts))
		else:
			print "@@@@@@@@@@@channel: ", channel
			raise TypeError("Number of channels must be 1, 2, 3 or 4!")

	def setHorizontalScale(self, seconds):
		self.sendCommand("HOR:MAI:SCA " + str(seconds))

	def adjustVerticalScale(self, channel):
		if 'int' in repr(type(channel)) and channel in self.__channels:
			sch = str(channel)
			
			self.setVerticalScale(channel, self.MAX_VOLTS_PER_DIV)
			sleep(2)
			#Fourth approximation is optimal
			prevCoarse = None
			for i in range(3):
				coarseAmp = self.getAmplitude(channel)
				coarseAmp = 1.3*coarseAmp
				if 'float' in repr(type(prevCoarse)) and (abs(prevCoarse - coarseAmp) < 0.1*coarseAmp):
					return
				else:
					prevCoarse = coarseAmp
					unitsPerDivision = coarseAmp/8
					self.setVerticalScale(channel, unitsPerDivision)

					#Wait for oscilloscope to measure amplitude
					sleep(0.5)
		else:
			raise TypeError("There are only 4 channels in the oscilloscope!")

	def adjustHorizontalScale(self, freq, funcType = "sine"):
		if funcType == "square":
			pass
		else:
			unitsPerDivision = 1/freq
			self.setHorizontalScale(unitsPerDivision)
			sleep(0.1)

	def getAmplitude(self, channel):
		if 'int' in repr(type(channel)) and channel in self.__channels:
			sch = str(channel)
			
			self.sendCommand("MEASU:MEAS1:STATE ON")
			self.sendCommand("MEASU:MEAS1:TYP AMP")
			self.sendCommand("MEASU:MEAS1:SOURCE1 CH" + sch)
			sleep(0.4) #Wait for oscilloscope to measure value
			
			self.sendCommand("MEASU:MEAS1:VAL?")

			return float(self.readResponse())
		else:
			raise Exception("There are only 4 channels in this oscilloscope!")


	def getPhaseShift(self, channelFrom, channelTo):
		if 'int' in repr(type(channelFrom)) and 'int' in repr(type(channelTo)) and channelFrom in self.__channels and channelTo in self.__channels:
			sch1 = str(channelFrom)
			sch2 = str(channelTo)

			self.sendCommand("MEASU:MEAS2:STATE ON")
			self.sendCommand("MEASU:MEAS2:TYP PHA")
			self.sendCommand("MEASU:MEAS2:SOURCE1 " + sch1)
			self.sendCommand("MEASU:MEAS2:SOURCE2 " + sch2)
			
			n = 5
			from numpy import mean, zeros
			tab = zeros(n)
			for i in range(n):
				sleep(0.4)
				self.sendCommand("MEASU:MEAS2:VAL?")
				phaseShift = float(self.readResponse())
				tab[i] = phaseShift

			return (mean(tab)) * math.pi/180
		else:
			raise Exception("There are only 4 channels in this oscilloscope!")

if __name__ == "__main__":
	print "Testing oscillsocope."
	osc = Oscilloscope()
	print osc.getID()
	print "----------------------------------------------------"
	# print "Adjusting vertical scale on channel 1..."
	# osc.adjustVerticalScale(1)
	# print "Amplitude, channel 1:", osc.getAmplitude(1)
	# print "Adjusting vertical scale on channel 2..."
	# osc.adjustVerticalScale(2)
	# print "Amplitude, channel 2:", osc.getAmplitude(2)
	# print "Adjusting horizontal scale..."
	# osc.adjustHorizontalScale(1e3)
	# ps = osc.getPhaseShift(1, 2)
	# print "Phase shift:", ps

