#!/usr/bin/env python

import numpy as np
from Oscilloscope import Oscilloscope
from Generator import Generator
import matplotlib.pyplot as plt
import numpy as np
import serial

class MeasurementController:
	def __init__(self, \
				Oscilloscope=None, \
				Generator=None):
		
		self.generator = Generator
		self.oscilloscope = Oscilloscope
		
	def setupMeasurement(self,\
				minFrequency=0, \
				maxFrequency=0, \
				powerBase=0, \
				channelIn=None, \
				channelOut=None):
		self.chIn = channelIn
		fmin, fmax, steps = self.__valuesValidator(minFrequency,maxFrequency, powerBase)
		logmin = np.log10(fmin)
		logmax = np.log10(fmax)
		self.logspace =	np.logspace(logmin, logmax, powerBase)
		self.chOut = channelOut
		self.position = 0
		self.generator.generate_signal("SINUSOID", minFrequency, 1)
		self.inAmpl = self.__getInputAmplitude(self.chIn)
	
	
	def getAmpPoint(self):
		try:
			freqIn = self.logspace[self.position]
		except IndexError:
			return 'stop'
		
		self.generator.generate_signal("SINUSOID", freqIn, self.inAmpl)
		self.oscilloscope.adjustHorizontalScale(freqIn)
		self.oscilloscope.adjustVerticalScale(self.chOut)
		amplitude = self.oscilloscope.getAmplitude(self.chOut)
		self.position += 1
		return self.logspace[self.position-1], amplitude
		
		
	def getPhasePoint(self):
		try:
			freqIn = self.logspace[self.position]
		except IndexError:
			return 'stop'

		self.generator.generate_signal("SINUSOID", freqIn, self.inAmpl)
		self.oscilloscope.adjustHorizontalScale(freqIn)
		self.oscilloscope.adjustVerticalScale(self.chOut)
		self.oscilloscope.adjustVerticalScale(self.chIn)
		phaseShift = self.oscilloscope.getPhaseShift(self.chIn, self.chOut)
		self.position += 1
		return self.logspace[self.position-1], phaseShift

	
	def __getInputAmplitude(self, chIn):
		self.oscilloscope.adjustHorizontalScale(self.logspace[self.position])
		self.oscilloscope.adjustVerticalScale(self.chIn)
		amplitude = self.oscilloscope.getAmplitude(self.chIn)
		return amplitude
		
		
	def __valuesValidator(self, fmin, fmax, steps):
		if fmin < 0.0001:
			print "WARNING: minimal frequency is too small; will be shifted to 1 mHz"
			fmin = 0.001
		if fmax > 14999999:
			print "WARNING: maximal frequency is too large; will be shifted to 14 MHz"
			fmin = 14000000
		if steps > 1000:
			print "WARNING: too much steps to handle; will be shifted to 1000 steps"
			steps = 1000
		return fmin, fmax, steps
		

	
if __name__=='__main__':
	pass
	'''measCntrl = MeasurementController(signalType="SINUSOID", \
				minFrequency=1, \
				maxFrequency=4, \
				numberOfSteps=30, \
				inputAmplitude=1, \
				channelIn=1, \
				channelOut=2, \
				portOscilloscope='/dev/ttyUSB1', \
				portGenerator='/dev/ttyUSB0')
	measCntrl.getAmpCharacteristic()
	'''
