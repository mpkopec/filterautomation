#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
import traceback

class Logger:
	__log_data = None
	__log_types = None


	def __init__ (self):
		self.__log_types = ['info', 'warning', 'error']

		self.__log_data = []

		self.log("Logging started")


	def log (self, msg, type = 'info'):
		curr_date = datetime.now().isoformat().replace("T", " ")
		if type in self.__log_types:
			entry = [curr_date, type, msg]
			self.__log_data.append(entry)
		else:
			entry = [curr_date, 'error', "Calling log method with wrong type parameter.\nMessage: \n  " + msg + ".\n\nTraceback: \n" + "\n".join(traceback.format_stack())]
			self.__log_data.append(entry)


	def get_log_string (self):
		log_string = ""

		for entry in self.__log_data:
			log_string = log_string + "[" + entry[0] + "] [" + entry[1] + "] " + entry[2] + \
						 "\n-------------------------------------\n"

		return log_string


	def save_log_string (self, filename):
		fhandle = open(filename, "w")
		fhandle.write(self.get_log_string())
		fhandle.close()

if __name__ == '__main__':
	logger = Logger()
	logger.log("Program started.")
	logger.log("Ooooops", "lol")
	logger.log("Basic error", 'error')
	logger.log("Basic warning", 'warning')

	print logger.get_log_string()
	logger.save_log_string("test_log.log")