#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import glob
import serial

from Oscilloscope import Oscilloscope
from Generator import Generator
from MeasurementController import MeasurementController

class Config():
	osc = None
	oscConnParams = None
	gen = None
	genConnParams = None
	measParams = None
	measCtrl = None


	def __init__(self):
		self.osc = Oscilloscope()
		self.gen = Generator()
		self.measCtrl = MeasurementController(self.osc, self.gen)

		self.oscConnParams = {}
		self.genConnParams = {}
		self.measParams = {}


	def debug (self):
		print self.__dict__


	def list_serial_ports(self):
		if sys.platform.startswith('win'):
			ports = ['COM' + str(i + 1) for i in range(256)]

		elif sys.platform.startswith('linux'):
			# this is to exclude your current terminal "/dev/tty"
			ports = glob.glob('/dev/tty[A-Za-z]*')

		elif sys.platform.startswith('darwin'):
			ports = glob.glob('/dev/tty.*')

		else:
			raise EnvironmentError('Unsupported platform')

		result = []
		for port in ports:
			try:
				s = serial.Serial(port)
				s.close()
				result.append(port)
			except (OSError, serial.SerialException):
				pass
		return result