#!/usr/bin/env python
# coding=utf-8

from __future__ import division

from PyQt4 import QtCore, QtGui
import numpy as np
from BodePlots import BodePlots
from SnapShot import SnapShot

class AmplitudeThread(QtCore.QThread):
	__x = []
	__y = []
	__config = None
	__simConfig = None
	__simulationMode = True

	def __init__(self, config, simConfig, simulationMode = True):
		QtCore.QThread.__init__(self)
		self.__config = config
		self.__simConfig = simConfig
		self.__simulationMode = simulationMode
		
		
	def setSimulationMode(self, simulationMode):
		self.__simulationMode = simulationMode


	def run(self):
		print "Amplitude response thread"
		print "Simulation mode:", self.__simulationMode
		self.__x = []
		self.__y = []
		if not self.__simulationMode:
			self.__config.measCtrl.setupMeasurement(self.__config.measParams["startFreq"],
													self.__config.measParams["stopFreq"],
													self.__config.measParams["powerBase"],
													self.__config.measParams["inCh"],
													self.__config.measParams["outCh"])
			steps = len(self.__config.measCtrl.logspace)
			for i in range(steps):
				if self.__config.cancelFlag:
					self.__config.cancelFlag = False
					self.emit(QtCore.SIGNAL("updateProgressBar"), 0)
					self.emit(QtCore.SIGNAL("cancelledMeasurement"))
					return

				(x, y) = self.__config.measCtrl.getAmpPoint()
				y = 20 * np.log10(y / self.__config.measCtrl.inAmpl)
				self.__x.append(x)
				self.__y.append(y)
				self.emit(QtCore.SIGNAL("updateProgressBar"), (i+1)/steps)
		else:
			from time import sleep

			n = 10
			for i in range(n):
				if self.__config.cancelFlag:
					self.__config.cancelFlag = False
					self.emit(QtCore.SIGNAL("updateProgressBar"), 0)
					self.emit(QtCore.SIGNAL("cancelledMeasurement"))
					return
				else:
					sleep(0.5)
				self.emit(QtCore.SIGNAL("updateProgressBar"), (i+1)/n)

			bode_plots = BodePlots(	self.__simConfig.simParams["startFreq"],
									self.__simConfig.simParams["stopFreq"],
									self.__simConfig.simParams["powerBase"],
									self.__simConfig.simParams["outCh"])
			self.__x, self.__y = bode_plots.get_magnitude_plot()

		self.emit(QtCore.SIGNAL("ampResponseReady"), self.__x, self.__y)


class SnapshotThread(QtCore.QThread):
	__x = []
	__y = []
	__config = None
	__simConfig = None
	__simulationMode = True

	def __init__(self, config, simConfig, simulationMode = True):
		QtCore.QThread.__init__(self)
		self.__config = config
		self.__simConfig = simConfig
		self.__simulationMode = simulationMode
		
		
	def setSimulationMode(self, simulationMode):
		self.__simulationMode = simulationMode


	def run(self):
		print "Snapshot thread"
		print "Simulation mode:", self.__simulationMode
		if not self.__simulationMode:
			self.__config.measCtrl.setupMeasurement(self.__config.measParams["startFreq"],
													self.__config.measParams["stopFreq"],
													self.__config.measParams["powerBase"],
													self.__config.measParams["inCh"],
													self.__config.measParams["outCh"])

			snapShot = SnapShot(self.__config.osc, self.__config.measParams["inCh"])
			self.__x, self.__y = snapShot.getSnapShot()
			self.emit(QtCore.SIGNAL("updateProgressBar"), 1)
		else:
			from time import sleep

			n = 10
			for i in range(n):
				if self.__config.cancelFlag:
					self.__config.cancelFlag = False
					self.emit(QtCore.SIGNAL("updateProgressBar"), 0)
					self.emit(QtCore.SIGNAL("cancelledMeasurement"))
					return
				else:
					sleep(0.5)
					
				self.emit(QtCore.SIGNAL("updateProgressBar"), (i+1)/n)

			self.__x = np.linspace(self.__simConfig.simParams["startFreq"],
							self.__simConfig.simParams["stopFreq"],
							self.__simConfig.simParams["powerBase"])
			import random
			self.__y = [np.sin(i)+random.random()/4 for i in self.__x ]

		self.emit(QtCore.SIGNAL("snapshotReady"), self.__x, self.__y)