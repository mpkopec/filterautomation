#!/usr/bin/env python
# coding=utf-8

from __future__ import division

from PyQt4 import QtCore, QtGui
import MainWindow
import sys
import serial
import numpy as np

from SimConfig import SimConfig
from Config import Config
from Logger import Logger
from Oscilloscope import Oscilloscope
from Generator import Generator
from SnapShot import SnapShot
from Threads import *


def errorBox(text, extendedText = ""):
	msgBox = QtGui.QMessageBox()
	msgBox.setWindowTitle("Error!")
	msgBox.setText(text)
	msgBox.setDetailedText(extendedText)
	msgBox.exec_()

def infoBox(text, extendedText = ""):
	msgBox = QtGui.QMessageBox()
	msgBox.setWindowTitle("Info")
	msgBox.setText(text)
	msgBox.setDetailedText(extendedText)
	msgBox.exec_()


class MainGUIController(QtGui.QMainWindow):
	__mw = None
	__config = None
	__simConfig = None
	__logger = None
	__app = None
	__simulationMode = True
	__config = Config()
	__ampThread = None
	__snapThread = None

	def __init__(self, appInstance):
		super(MainGUIController, self).__init__()
		self.__mw = MainWindow.Ui_MainWindow()
		self.__mw.setupUi(self)
		self.__logger = Logger()
		self.__app = appInstance
		self.__config.cancelFlag = False

		if self.__simulationMode:
			self.__mw.oscPortChoice.insertItems(0, ["Simulation mode"])
			self.__mw.genPortChoice.insertItems(0, ["Simulation mode"])
			self.__mw.oscPortChoice.setEnabled(False)
			self.__mw.genPortChoice.setEnabled(False)

			self.__simConfig = SimConfig()
			self.__simConfig.simParams["inCh"] = self.__mw.inputChannelChoice.currentIndex() + 1
			self.__simConfig.simParams["outCh"] = self.__mw.outputChannelChoice.currentIndex() + 1
			self.__simConfig.simParams["startFreq"] = float(self.__mw.startFreqField.text())
			self.__simConfig.simParams["stopFreq"] = float(self.__mw.stopFreqField.text())
			self.__simConfig.simParams["powerBase"] = float(self.__mw.numberOfSteps.text())

			self.__simConfig.debug()

		else:
			self.initInstruments()

		self.__ampThread = AmplitudeThread(simulationMode=self.__simulationMode, config=self.__config, simConfig=self.__simConfig)
		self.__snapThread = SnapshotThread(simulationMode=self.__simulationMode, config=self.__config, simConfig=self.__simConfig)
		# Oscilloscope section
		self.__mw.oscPortChoice.currentIndexChanged.connect(self.changeOscPort)
		self.__mw.oscBaudChoice.currentIndexChanged.connect(self.changeOscBaud)
		self.__mw.oscCheckButton.clicked.connect(self.testOsc)
		# Generator section
		self.__mw.genPortChoice.currentIndexChanged.connect(self.changeGenPort)
		self.__mw.genBaudChoice.currentIndexChanged.connect(self.changeGenBaud)
		self.__mw.genCheckButton.clicked.connect(self.testGen)
		# Oscilloscope config section
		self.__mw.inputChannelChoice.currentIndexChanged.connect(self.changeInputChannel)
		self.__mw.outputChannelChoice.currentIndexChanged.connect(self.changeOutputChannel)
		# Measurement config section
		self.__mw.startFreqField.editingFinished.connect(self.changeStartFreq)
		self.__mw.stopFreqField.editingFinished.connect(self.changeStopFreq)
		self.__mw.numberOfSteps.editingFinished.connect(self.changePowerBase)
		# Measurement triggering buttons
		self.__mw.ampResponseButton.clicked.connect(self.ampResponseButtonClicked)
		self.__mw.toggleSimulationMode.clicked.connect(self.toggleSimulationMode)
		self.__mw.getSnapshotButton.clicked.connect(self.snapshotButtonClicked)
		# Dataset ready signals from outer threads
		self.connect(self.__ampThread, QtCore.SIGNAL("ampResponseReady"), self.plotAmpResponse)
		self.connect(self.__snapThread, QtCore.SIGNAL("snapshotReady"), self.plotSnapshot)
		# Update progressbar signal from outer threads
		self.connect(self.__ampThread, QtCore.SIGNAL("updateProgressBar"), self.updateProgressBar)
		self.connect(self.__snapThread, QtCore.SIGNAL("updateProgressBar"), self.updateProgressBar)
		# Cancel button handling with outer threads
		self.connect(self.__ampThread, QtCore.SIGNAL("cancelledMeasurement"), self.cancelledMeasurement)
		self.connect(self.__snapThread, QtCore.SIGNAL("cancelledMeasurement"), self.cancelledMeasurement)
		# Cancel button
		self.__mw.cancelButton.clicked.connect(self.cancelOperation)

		# Menu actions
		self.__mw.actionClose.triggered.connect(self.closeWindow)
		self.__mw.actionSave_amplitude_figure.triggered.connect(self.saveAmp)
		self.__mw.actionSave_snapshot.triggered.connect(self.saveSnap)
		self.enableGUI()

		self.show()


	def disableGUI(self):
		self.__mw.genBaudChoice.setEnabled(False)
		self.__mw.oscBaudChoice.setEnabled(False)
		self.__mw.oscCheckButton.setEnabled(False)
		self.__mw.genCheckButton.setEnabled(False)
		self.__mw.inputChannelChoice.setEnabled(False)
		self.__mw.outputChannelChoice.setEnabled(False)
		self.__mw.startFreqField.setEnabled(False)
		self.__mw.stopFreqField.setEnabled(False)
		self.__mw.numberOfSteps.setEnabled(False)
		self.__mw.ampResponseButton.setEnabled(False)
		self.__mw.getSnapshotButton.setEnabled(False)
		self.__mw.toggleSimulationMode.setEnabled(False)
		self.__mw.plotArea.setEnabled(False)
		self.__mw.menuFile.setEnabled(False)
		self.__mw.menuHelp.setEnabled(False)
		self.__mw.cancelButton.setEnabled(True)


	def enableGUI(self):
		self.__mw.genBaudChoice.setEnabled(True)
		self.__mw.oscBaudChoice.setEnabled(True)
		self.__mw.oscCheckButton.setEnabled(True)
		self.__mw.genCheckButton.setEnabled(True)
		self.__mw.inputChannelChoice.setEnabled(True)
		self.__mw.outputChannelChoice.setEnabled(True)
		self.__mw.startFreqField.setEnabled(True)
		self.__mw.stopFreqField.setEnabled(True)
		self.__mw.numberOfSteps.setEnabled(True)
		self.__mw.ampResponseButton.setEnabled(True)
		self.__mw.getSnapshotButton.setEnabled(True)
		self.__mw.toggleSimulationMode.setEnabled(True)
		self.__mw.plotArea.setEnabled(True)
		self.__mw.menuFile.setEnabled(True)
		self.__mw.menuHelp.setEnabled(True)
		self.__mw.cancelButton.setEnabled(False)


	def initInstruments (self):
		comPorts = self.__config.list_serial_ports()
		self.__mw.oscPortChoice.clear()
		self.__mw.oscPortChoice.insertItems(0, comPorts if comPorts else ["No ports available"])

		self.__mw.genPortChoice.clear()
		self.__mw.genPortChoice.insertItems(0, comPorts if comPorts else ["No ports available"])
		if self.__mw.oscPortChoice.currentText() == "No ports available":
			self.__mw.oscPortChoice.setEnabled(False)
		if self.__mw.genPortChoice.currentText() == "No ports available":
			self.__mw.genPortChoice.setEnabled(False)


		self.__config.oscConnParams["baud"] = int(self.__mw.oscBaudChoice.currentText())
		self.__config.oscConnParams["port"] = str(self.__mw.oscPortChoice.currentText())
		if self.__mw.oscPortChoice.currentText() != "No ports available":
			self.renewOsc()

		self.__config.genConnParams["baud"] = int(self.__mw.genBaudChoice.currentText())
		self.__config.genConnParams["port"] = str(self.__mw.genPortChoice.currentText())
		if self.__mw.oscPortChoice.currentText() != "No ports available":
			self.renewGen()

		self.__config.measParams["inCh"] = self.__mw.inputChannelChoice.currentIndex() + 1
		self.__config.measParams["outCh"] = self.__mw.outputChannelChoice.currentIndex() + 1
		self.__config.measParams["startFreq"] = float(self.__mw.startFreqField.text())
		self.__config.measParams["stopFreq"] = float(self.__mw.stopFreqField.text())
		self.__config.measParams["powerBase"] = float(self.__mw.numberOfSteps.text())

		self.__config.debug()


	def closeWindow(self):
		self.close()


	def renewOsc(self):
		if self.__simulationMode:
			pass
		else:
			self.__config.debug()
			self.__config.osc = Oscilloscope(self.__config.oscConnParams["port"], self.__config.oscConnParams["baud"])
			self.__config.measCtrl.oscilloscope = self.__config.osc


	def renewGen(self):
		if self.__simulationMode:
			pass
		else:
			self.__config.gen = Generator(self.__config.genConnParams["port"], self.__config.genConnParams["baud"])
			self.__config.measCtrl.generator = self.__config.gen


	def changeOscPort(self):
		self.__config.oscConnParams["port"] = str(self.__mw.oscPortChoice.currentText())
		self.__mw.oscCheckField.clear()

		try:
			self.renewOsc()
		except serial.SerialException:
			self.__logger.log("Invalid port for oscilloscope", "error")

		self.__config.debug()


	def changeOscBaud(self):
		if self.__simulationMode:
			pass
		else:
			self.__config.oscConnParams["baud"] = int(self.__mw.oscBaudChoice.currentText())
			self.__mw.oscCheckField.clear()

			try:
				self.renewOsc()
			except serial.SerialException:
				self.__logger.log("Invalid baud rate for oscilloscope", "error")

			self.__config.debug()



	def testOsc(self):
		if self.__simulationMode:
			self.__mw.oscCheckField.clear()
			self.__mw.oscCheckField.insert('oscilloscope simulation')
		else:
			self.__mw.oscCheckField.clear()
			self.__mw.oscCheckField.insert(self.__config.osc.identify())
			self.__mw.oscCheckField.home(False)


	def changeGenPort(self):
		self.__config.genConnParams["port"] = str(self.__mw.genPortChoice.currentText())
		self.__mw.genCheckField.clear()

		try:
			self.renewGen()
		except serial.SerialException:
			self.__logger.log("Invalid port for generator", "error")

		self.__config.debug()


	def changeGenBaud(self):
		if self.__simulationMode:
			pass
		else:
			self.__config.genConnParams["baud"] = int(self.__mw.genBaudChoice.currentText())
			self.__mw.genCheckField.clear()

			try:
				self.renewGen()
			except serial.SerialException:
				self.__logger.log("Invalid baud rate for generator", "error")

			self.__config.debug()


	def testGen(self):
		if self.__simulationMode:
			self.__mw.genCheckField.clear()
			self.__mw.genCheckField.insert('signal generator simulation')
		else:
			self.__mw.genCheckField.clear()
			self.__mw.genCheckField.insert(self.__config.gen.identify())
			self.__mw.genCheckField.home(False)


	def changeInputChannel(self):
		if self.__simulationMode:
			newInCh = self.__mw.inputChannelChoice.currentIndex() + 1

			if self.__simConfig.simParams["outCh"] != newInCh:
				self.__simConfig.simParams["inCh"] = newInCh
			else:
				errorBox("Wrong input channel!", "Input channel must be different than the output channel, please enter such."
												 " Your change was not saved.")

			self.__simConfig.debug()
		else:
			newInCh = self.__mw.inputChannelChoice.currentIndex() + 1

			if self.__config.measParams["outCh"] != newInCh:
				self.__config.measParams["inCh"] = newInCh
			else:
				errorBox("Wrong input channel!", "Input channel must be different than the output channel, please enter such."
												 " Your change was not saved.")

			self.__config.debug()


	def changeOutputChannel(self):
		if self.__simulationMode:
			newOutCh = self.__mw.outputChannelChoice.currentIndex() + 1

			if self.__simConfig.simParams["inCh"] != newOutCh:
				self.__simConfig.simParams["outCh"] = self.__mw.outputChannelChoice.currentIndex() + 1
			else:
				errorBox("Wrong output channel!", "Output channel must be different than the input channel, please enter such."
												  " Your change was not saved.")

			self.__simConfig.debug()
		else:
			newOutCh = self.__mw.outputChannelChoice.currentIndex() + 1

			if self.__config.measParams["inCh"] != newOutCh:
				self.__config.measParams["outCh"] = self.__mw.outputChannelChoice.currentIndex() + 1
			else:
				errorBox("Wrong output channel!", "Output channel must be different than the input channel, please enter such."
												  " Your change was not saved.")

			self.__config.debug()


	def changeStartFreq(self):
		if self.__simulationMode:
			try:
				self.__simConfig.simParams["startFreq"] = float(self.__mw.startFreqField.text())
			except ValueError:
				errorBox("Wrong number given!", "Start frequency must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__simConfig.debug()
		else:
			try:
				self.__config.measParams["startFreq"] = float(self.__mw.startFreqField.text())
			except ValueError:
				errorBox("Wrong number given!", "Start frequency must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__config.debug()


	def changeStopFreq(self):
		if self.__simulationMode:
			try:
				self.__simConfig.simParams["stopFreq"] = float(self.__mw.stopFreqField.text())
			except ValueError:
				errorBox("Wrong number given!", "Stop frequency must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__simConfig.debug()
		else:
			try:
				self.__config.measParams["stopFreq"] = float(self.__mw.stopFreqField.text())
			except ValueError:
				errorBox("Wrong number given!", "Stop frequency must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__config.debug()


	def changePowerBase(self):
		if self.__simulationMode:
			try:
				self.__simConfig.simParams["powerBase"] = float(self.__mw.numberOfSteps.text())
			except ValueError:
				errorBox("Wrong number given!", "Power base must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__simConfig.debug()
		else:
			try:
				self.__config.measParams["powerBase"] = float(self.__mw.numberOfSteps.text())
			except ValueError:
				errorBox("Wrong number given!", "Power base must be a floating point number."
												" You may use only dot as decimal separator."
												" Scientific notation is allowed.")
			self.__config.debug()


	def cancelledMeasurement(self):
		self.enableGUI()
		self.__mw.ampResponseButton.setText("Measure amplitude response")
		self.__mw.getSnapshotButton.setText("Take a snapshot")


	def ampResponseButtonClicked(self):
		self.__mw.ampResponseButton.setText("Measurement in progress")
		self.__mw.progressBar.setValue(0)
		self.disableGUI()
		self.__mw.ampResponseButton.repaint()
		self.__ampThread.start()


	def plotAmpResponse(self, xarr, yarr):
		self.__mw.plotArea.setCurrentIndex(0)
		self.__mw.plotArea.repaint()

		self.__mw.amp.clf()
		self.__mw.ampChart.repaint()
		plot = self.__mw.amp.add_subplot(111)
		plot.cla()
		plot.set_title("Amplitude response")
		plot.set_xlabel("Frequency [Hz]")
		plot.set_ylabel("Magnitude [dB]")
		plot.set_xscale("log")
		plot.grid(b = True, which = "major", color = "#aaaaaa")
		plot.grid(b = True, which = "minor", color = "#eeeeee")
		plot.plot(xarr, yarr)
		self.__mw.ampChart.draw()
		self.__mw.ampChart.repaint()

		self.__mw.ampResponseButton.setText("Measure amplitude response")
		self.__mw.ampResponseButton.repaint()
		self.enableGUI()


	def updateProgressBar(self, val):
		self.__mw.progressBar.setValue(int(val*100))
		
		
	def renewThreads(self):
		if not self.__ampThread.isRunning():
			self.__ampThread.setSimulationMode(self.__simulationMode)
		if not self.__snapThread.isRunning():
			self.__snapThread.setSimulationMode(self.__simulationMode)
		## Dataset ready signals from outer threads
		#self.connect(self.__ampThread, QtCore.SIGNAL("ampResponseReady"), self.plotAmpResponse)
		#self.connect(self.__snapThread, QtCore.SIGNAL("snapshotReady"), self.plotSnapshot)
		## Update progressbar signal from outer threads
		#self.connect(self.__ampThread, QtCore.SIGNAL("updateProgressBar"), self.updateProgressBar)
		#self.connect(self.__snapThread, QtCore.SIGNAL("updateProgressBar"), self.updateProgressBar)
		## Cancel button handling with outer threads
		#self.connect(self.__ampThread, QtCore.SIGNAL("cancelledMeasurement"), self.cancelledMeasurement)
		#self.connect(self.__snapThread, QtCore.SIGNAL("cancelledMeasurement"), self.cancelledMeasurement)


	def toggleSimulationMode(self):
		if self.__simulationMode:
			if self.__config.list_serial_ports():
				self.__simulationMode = False
				self.__mw.toggleSimulationMode.setText("Simulation mode - OFF")
				self.__mw.oscPortChoice.setEnabled(True)
				self.__mw.genPortChoice.setEnabled(True)
				self.initInstruments()
				self.renewThreads()
			else:
				errorBox("No serial devices available", "Neither of your serial ports have active device, exiting simulation mode is not possible.")
		else:
			self.__simulationMode = True
			self.__mw.toggleSimulationMode.setText("Simulation mode - ON")
			self.__mw.oscPortChoice.setEnabled(False)
			self.__mw.genPortChoice.setEnabled(False)
			self.renewThreads()


	def snapshotButtonClicked(self):
		self.__mw.getSnapshotButton.setText("Downloading data...")
		self.__mw.progressBar.setValue(0)
		self.disableGUI()
		self.__mw.getSnapshotButton.repaint()
		self.__snapThread.start()


	def plotSnapshot(self, x, y):
		self.__mw.plotArea.setCurrentIndex(1)
		self.__mw.plotArea.repaint()
		self.blockSignals(True)
		self.__mw.snap.clf()
		plot = self.__mw.snap.add_subplot(111)
		plot.cla()
		plot.set_title("Oscilloscope snapshot, channel " + str(self.__simConfig.simParams["inCh"]))
		plot.set_xlabel("Time [ms]")
		plot.set_ylabel("Voltage [V]")
		plot.plot(x, y)
		self.__mw.snapChart.draw()

		self.__mw.getSnapshotButton.setText("Take a snapshot")
		self.enableGUI()
		self.repaint()


	def cancelOperation(self):
		if self.__ampThread.isRunning() or (self.__snapThread.isRunning() and self.__simulationMode):
			self.__config.cancelFlag = True
		print self.__config.debug()


	def saveAmp(self):
		self.saveFigure("amp")


	def saveSnap(self):
		self.saveFigure("snap")


	def saveFigure(self, which):
		if which == 'amp':
			filename = QtGui.QFileDialog.getSaveFileName(self, "Save amplitude figure as a *.png file", filter="Image Files (*.png)")

			try:
				if filename:
					self.__mw.amp.savefig(str(filename))
					infoBox("File saved!")
			except Exception:
				errorBox("Error writing file!")
		else:
			filename = QtGui.QFileDialog.getSaveFileName(self, "Save snapshot as a *.png file", filter="Image Files (*.png)")

			try:
				if filename:
					self.__mw.snap.savefig(str(filename))
					infoBox("File saved!")
			except Exception as e:
				errorBox("Error writing file!")



app = QtGui.QApplication(sys.argv)
window = MainGUIController(app)
app.processEvents()
sys.exit(app.exec_())
