#!/usr/bin/env python

import serial
from Instrument import Instrument

class Generator(Instrument):

	__allowedSignals = ("SINUSOID", "SQUARE", "TRIANGLE", "RAMP", "NOISE", "DC", "USER")
	
	def __init__(self,\
				port = '/dev/ttyUSB0',\
				baudrate = 9600):
		Instrument.__init__(self,\
							port,\
							baudrate,\
							timeout = 1,\
							bitcount = serial.EIGHTBITS,\
							stopbits = serial.STOPBITS_TWO,\
							parity = serial.PARITY_NONE,\
							terminator = 1)
		
		self.sendMultipleCommands(["SYST:LOC","SYST:REM"])
	
	def generate_signal(self, signalType, frequency, amplitude):
		if not self.__valuesValidator(signalType, frequency, amplitude):
			raise Exception("Arguments not allowed")
		self.sendCommand("APPLY:"+signalType+" "+str(frequency)+","+str(amplitude))
		
	def __valuesValidator(self,signal, freq, ampl):
		if type(signal) is not str or signal not in self.__allowedSignals:
			return 0
		__numericTypes = (float, int)
		if type(freq) not in __numericTypes or type(ampl) not in __numericTypes:
			return 1
		return 1
		
if __name__=='__main__':
	g=Generator()
	g.generate_signal("SINUSOID",1500,1.5)
	
