\contentsline {section}{\numberline {1\relax .\kern .5em }U\IeC {\.z}ytkowanie aplikacji}{2}{section.1}
\contentsline {subsection}{\numberline {1.1\relax .\kern .5em }Okno aplikacji}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2\relax .\kern .5em }Ustawienia transmisji}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3\relax .\kern .5em }Ustawienia kana\IeC {\l }\IeC {\'o}w}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4\relax .\kern .5em }Ustawienia pomiaru}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5\relax .\kern .5em }Pomiar charakterystyki amplitudowej}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6\relax .\kern .5em }Zrzut ekranu z\nobreakspace {}oscyloskopu (Snapshot)}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7\relax .\kern .5em }Tryb symulacji}{5}{subsection.1.7}
\contentsline {section}{\numberline {2\relax .\kern .5em }Implementacja}{7}{section.2}
\contentsline {subsection}{\numberline {2.1\relax .\kern .5em }Warstwa komunikacji --- klasy \texttt {Instrument}, \texttt {Oscilloscope} i\nobreakspace {}\texttt {Generator}}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2\relax .\kern .5em }Warstwa pomiaru --- klasa \texttt {MeasurementController}}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3\relax .\kern .5em }Zrzut danych z\nobreakspace {}oscyloskopu --- klasa \texttt {SnapShot}}{8}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4\relax .\kern .5em }Wsp\IeC {\'o}\IeC {\l }praca z\nobreakspace {}GUI}{9}{subsection.2.4}
