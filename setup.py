#!/usr/bin/env python

from setuptools import setup

setup(	name 		= "FilterAutomation",
		version 	= "1.0",
		description = "Application for automation of analog filter characteristics measurement",
		author 		= "Arkadiusz Kisiel, Maciej Kopec",
		py_modules 	= [ 'FilterAutomation.BodePlots',
					    'FilterAutomation.Config',
						'FilterAutomation.FilterApp',
						'FilterAutomation.Generator',
						'FilterAutomation.Instrument',
						'FilterAutomation.Logger',
						'FilterAutomation.MainWindow',
						'FilterAutomation.MeasurementController',
						'FilterAutomation.Oscilloscope',
						'FilterAutomation.SimConfig',
						'FilterAutomation.SnapShot',
						'FilterAutomation.Threads' ],
		package_dir = { 'FilterAutomation': './src' },
		scripts 	= [ 'run-script/FilterAutomation' ]
)
